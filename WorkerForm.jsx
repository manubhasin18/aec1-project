import { Button, Col, Form, Image, Input, Modal, Row, Select, Typography, message } from 'antd';
import _ from 'lodash';
import Dragger from 'antd/lib/upload/Dragger';
import React, { useEffect, useState } from 'react';
import { connect, FormattedMessage } from 'umi';
import styles from './workers.less';
import pic from '../../assets/default-pic.png';


const WorkerForm = ({
  loading,
  workers,
  selectedWorker,
  setSelectedWorker,
  dispatch,
  fetchData,
  sites,
}) => {
  const [identityType, setIdentityType] = useState(selectedWorker.nidType);
  const [, setNidErrorMsg] = useState('');
  const [form] = Form.useForm();
  const [isEdited, setEdited] = useState(false);
  const [image, setImage] = useState('');
  const { workerProfile } = workers;
  const { parentSiteData } = sites;

  useEffect(() => {
    if (workerProfile?.profilePicUrl == null) {
      setImage(pic);
    } else {
      setImage(workerProfile.profilePicUrl);
    }
    switch (identityType) {
      case null:
        setNidErrorMsg('');
        break;
      case true:
        setNidErrorMsg(
          <FormattedMessage
            id="s4.workerform.NIN-starts-with-1-and-should-be-only-numbers.errormsg"
            defaultMessage="NIN starts with 1 and should be only numbers"
          />,
        );
        break;
      case false:
        setNidErrorMsg(
          <FormattedMessage
            id="s4.workerform.IQAMA-starts-with-2-and-should-be-only-numbers.errormsg"
            defaultMessage="IQAMA starts with 2 and should be only numbers"
          />,
        );
        break;
      default:
        setNidErrorMsg('');
    }
    if (identityType !== true || identityType !== false) form.validateFields(['nid']);
  }, [form, identityType]);

  const addWorker = (values) => {
    if (selectedWorker?.id) {
      const touchedFields = {};
      _.map(form.getFieldsValue(true), (v, k) => {
        if (form.isFieldTouched(k)) {
          touchedFields[k] = v;
        }
      });
      if (_.isEmpty(touchedFields)) {
        return;
      }
      dispatch({
        type: 'workers/updateWorker',
        workerData: { ...touchedFields, id: selectedWorker.id },
      }).then((success) => {
        if (success) {
          setSelectedWorker({ modalVisible: false });
          fetchData();
        }
      });
    } else {
      dispatch({
        type: 'workers/addWorker',
        workerData: { ...values, isActive: true, baseSiteId: sites.parentSiteData.id },
      }).then((success) => {
        if (success) {
          setSelectedWorker({ modalVisible: false });
          fetchData();
        }
      });
    }
  };

  const getAddButtonText = () => {
    if (!selectedWorker?.id) {
      if (loading.effects['workers/addWorker']) {
        return <FormattedMessage id="s4.workerform.loading" defaultMessage="Loading" />;
      }
      return <FormattedMessage id="s4.workerform.submit" defaultMessage="Submit" />;
    }
    if (loading.effects['workers/addWorker']) {
      return <FormattedMessage id="s4.workerform.loading" defaultMessage="Loading" />;
    }
    return <FormattedMessage id="s4.workerform.Update" defaultMessage="Update" />;
  };

  const onOverlayImageSelect = ({ file }) => {
    const fileData = new FormData();
    fileData.append('file', file);
    dispatch({ type: 'workers/uploadWorkerPic', fileData }).then((response) => {
      // saving url got from the image server
      setImage(response?.url);
      setEdited(true);
      form.setFieldsValue({
        profilePicUrl: response?.url,
      });
    });
  };

  function beforeUpload(file) {
    const isPNG = file.type === 'image/png';
    if (!isPNG) {
      message.error(
        <FormattedMessage
          id="s4.workerform.image.format"
          defaultMessage="The Image format must be png"
        />,
      );
    }
    const fileSize = file.size < 500000;
    if (!fileSize) {
      message.error(
        <FormattedMessage
          id="s4.workerform.image.size"
          defaultMessage="The Image Size must not be more that 500KB!"
        />,
      );
    }
    return fileSize && isPNG;
  }

  return (
    <>
      <Modal
        title={
          !selectedWorker?.id ? (
            <FormattedMessage id="worker.form.add.title" defaultMessage="Add Worker" />
          ) : (
            <FormattedMessage id="s4.workerform.Update.title" defaultMessage="Update Worker" />
          )
        }
        visible={selectedWorker.modalVisible}
        onCancel={() => {
          setIdentityType(null);
          setSelectedWorker({ modalVisible: false });
        }}
        width="80vw"
        bodyStyle={{
          display: 'flex',
          flexDirection: 'column',
        }}
        footer={null}
        forceRender
      >
        <Form
          layout="vertical"
          name="basic"
          initialValues={{
            ...selectedWorker,
          }}
          form={form}
          onFinish={addWorker}
          onValuesChange={(changedValues) => {
            setEdited(true);
            if (_.has(changedValues, 'nidType')) {
              setIdentityType(changedValues.nidType);
              form.validateFields(['nid']);
            }
          }}
          preserve={false}
          key={`${selectedWorker.modalVisible}-${selectedWorker.id}`}
        >
          <>
            <Row>
              <Typography.Title level={5}>
                <FormattedMessage
                  id="s4.workerform.typography.employee-details.title"
                  defaultMessage="Worker Details"
                />
              </Typography.Title>
            </Row>

            <Row style={{ height: '10%' }}>
              <Col span={24}>
                <Image
                  src={image}
                  style={{ width: '83px', height: '70px', border: '4px solid #d9d9d9' }}
                />
                <Dragger
                  className={`${styles.myDragger} ant-btn-primary`}
                  maxCount="1"
                  customRequest={onOverlayImageSelect}
                  beforeUpload={beforeUpload}
                  showUploadList={false}
                  height="30%"
                >
                  {' '}
                  Change
                </Dragger>
                <Form.Item hidden={true} name="profilePicUrl">
                  <Input value={image} />
                </Form.Item>
              </Col>
	      <Col xs={24} sm={12} md={12} lg={8} xl={8}>
                <Form.Item
                  label={
                    <FormattedMessage
                      id="s4.workerform.site.label"
                      defaultMessage="site"
                    />
                  }
                  name="workerSite"
                  rules={[
                    {
                      required: true,
                      message: (
                        <FormattedMessage
                          id="s4.workerform.please-enter-site.message"
                          defaultMessage="Please enter site"
                        />
                      ),
                    },
                  ]}
                >
	    <Select
              style={{ width: 200 }}
              placeholder={
                <FormattedMessage
                  id="s4.dashboard.parent_selection.placeholder"
                  defaultMessage="Select Parent Site"
                />
              }
              options={sites?.parentSites?.map((s) => ({ label: s.name, value: s.id }))}
              value={`${sites?.parentSite}`}
              onChange={(v) => {
                dispatch({
                  type: 'sites/fetchParentSite',
                  id: v,
                });
              }}
            />	
	    </Form.Item>
	    </Col>	
            </Row>

            <Row gutter={8}>
              <Col xs={24} sm={12} md={12} lg={6} xl={6}>
                <Form.Item
                  label={
                    <FormattedMessage
                      id="s4.workerform.employee-id.label"
                      defaultMessage="Worker Id"
                    />
                  }
                  name="employeeId"
                  rules={[
                    {
                      required: true,
                      message: (
                        <FormattedMessage
                          id="s4.workerform.enter-employee-id.message"
                          defaultMessage="Enter employee Id"
                        />
                      ),
                    },
                    {
                      required: true,
                      message: (
                        <FormattedMessage
                          id="s4.workerform.enter-only-numbers-and-hyphen..message"
                          defaultMessage="Enter only numbers and hyphen."
                        />
                      ),
                      pattern: /^[0-9-]+$/,
                    },
                  ]}
                >
                  <Input />
                </Form.Item>
              </Col>
              <Col xs={24} sm={12} md={12} lg={6} xl={6}>
                <Form.Item
                  label={<FormattedMessage id="s4.workerform.name.label" defaultMessage="Name" />}
                  name="name"
                  rules={[
                    {
                      required: true,
                      message: (
                        <FormattedMessage
                          id="s4.workerform.enter-name.message"
                          defaultMessage="Enter name"
                        />
                      ),
                      pattern: /^[a-zA-Z _]+$/,
                    },
                    {
                      required: true,
                      max: 80,
                      message: 'Name can be only a maximum of 80 characters',
                    },
                  ]}
                >
                  <Input />
                </Form.Item>
              </Col>

              <Col xs={24} sm={12} md={12} lg={6} xl={6}>
                <Form.Item
                  label={
                    <FormattedMessage
                      id="s4.workerform.identity-type.label"
                      defaultMessage="Identity Type"
                    />
                  }
                  name="nidType"
                  rules={[
                    {
                      required: false,
                      message: (
                        <FormattedMessage
                          id="s4.workerform.please-select-identity-type.message"
                          defaultMessage="Please select Identity Type"
                        />
                      ),
                    },
                  ]}
                >
                  <Select
                    options={[
                      {
                        label: 'NIN',
                        value: true,
                      },
                      {
                        label: 'IQAMA',
                        value: false,
                      },
                    ]}
                    showSearch
                    filterOption={(input, option) =>
                      option.label.toLowerCase().indexOf(input.toLowerCase()) >= 0
                    }
                  />
                </Form.Item>
              </Col>
              <Col xs={24} sm={12} md={12} lg={6} xl={6}>
                <Form.Item
                  label={
                    <FormattedMessage
                      id="s4.workerform.identity-number.label"
                      defaultMessage="Identity Number"
                    />
                  }
                  name="nid"
                  dependencies={['nidType']}
                  rules={[
                    {
                      required: !!(identityType === true || identityType === false),
                      message: (
                        <FormattedMessage
                          id="s4.workerform.enter-identity-number.message"
                          defaultMessage="Enter Identity Number"
                        />
                      ),
                      pattern: identityType === true ? /^1[0-9]*$/ : /^2[0-9]*$/,
                    },
                    {
                      message: (
                        <FormattedMessage
                          id="s4.workerform.should-be-a-10-digit-number.message"
                          defaultMessage="Should be a 10 digit number"
                        />
                      ),
                      len: 10,
                    },
                  ]}
                >
                  <Input disabled={identityType === null || identityType === undefined} />
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={8}>
              <Col xs={24} sm={12} md={12} lg={6} xl={6}>
                <Form.Item
                  label={
                    <FormattedMessage
                      id="s4.workerform.company-id.label"
                      defaultMessage="Company Id"
                    />
                  }
                  name="companyId"
                  rules={[
                    {
                      message: (
                        <FormattedMessage
                          id="s4.workerform.enter-company-id.message"
                          defaultMessage="Enter Company Id"
                        />
                      ),
                      pattern: /^(?=.*[0-9])([a-zA-Z0-9_ -]+)$/,
                    },
                    {
                      max: 15,
                      message: (
                        <FormattedMessage
                          id="s4.workerform.company-id-can-be-only-a-maximum-of-15-characters.message"
                          defaultMessage="Company ID can be only a maximum of 15 characters."
                        />
                      ),
                    },
                  ]}
                >
                  <Input />
                </Form.Item>
              </Col>
              <Col xs={24} sm={12} md={12} lg={8} xl={8}>
                <Form.Item
                  label={
                    <FormattedMessage
                      id="s4.workerform.worker-type.label"
                      defaultMessage="Worker Type"
                    />
                  }
                  name="workerType"
                  rules={[
                    {
                      required: true,
                      message: (
                        <FormattedMessage
                          id="s4.workerform.please-enter-worker-type.message"
                          defaultMessage="Please enter worker type"
                        />
                      ),
                    },
                  ]}
                >
                  <Select
                    options={_.map(workers.workerTypes, (r) => ({
                      label: r.name,
                      value: r.id,
                    }))}
                    showSearch
                    filterOption={(input, option) =>
                      option.label.toLowerCase().indexOf(input.toLowerCase()) >= 0
                    }
                  />
                </Form.Item>
              </Col>         
			 
	   </Row>
		
            <Row>
              <Typography.Title level={5}>
                <FormattedMessage
                  id="s4.workerform.typography.personal-details.title"
                  defaultMessage="Personal Details"
                />
              </Typography.Title>
            </Row>
            <Row gutter={8}>
              <Col xs={24} sm={12} md={12} lg={6} xl={6}>
                <Form.Item
                  label={
                    <FormattedMessage id="s4.workerform.genders.label" defaultMessage="Gender" />
                  }
                  name="gender"
                >
                  <Select
                    options={[
                      {
                        label: 'Male',
                        value: 'Male',
                      },
                      {
                        label: 'Female',
                        value: 'Female',
                      },
                    ]}
                    showSearch
                    filterOption={(input, option) =>
                      option.label.toLowerCase().indexOf(input.toLowerCase()) >= 0
                    }
                  />
                </Form.Item>
              </Col>
              <Col xs={24} sm={12} md={12} lg={6} xl={6}>
                <Form.Item
                  label={
                    <FormattedMessage
                      id="s4.workerform.nationality.label"
                      defaultMessage="Nationality"
                    />
                  }
                  name="nationalityId"
                >
                  <Select
                    options={_.map(workers.nationalities, (r) => ({
                      label: r.name,
                      value: r.id,
                    }))}
                    showSearch
                    filterOption={(input, option) =>
                      option.label.toLowerCase().indexOf(input.toLowerCase()) >= 0
                    }
                  />
                </Form.Item>
              </Col>
              <Col xs={24} sm={12} md={12} lg={6} xl={6}>
                <Form.Item
                  label={
                    <FormattedMessage
                      id="s4.workerform.passport-number.label"
                      defaultMessage="Passport Number"
                    />
                  }
                  name="passportNumber"
                  rules={[
                    {
                      message: (
                        <FormattedMessage
                          id="s4.workerform.enter-only-numbers-and-alphabets..message"
                          defaultMessage="Enter only numbers and alphabets."
                        />
                      ),
                      pattern: /^[A-Za-z0-9 _]*[A-Za-z0-9][A-Za-z0-9 _]*$/,
                    },
                  ]}
                >
                  <Input maxLength="19" />
                </Form.Item>
              </Col>
              <Col xs={24} sm={12} md={12} lg={6} xl={6}>
                <Form.Item
                  label={
                    <FormattedMessage
                      id="s4.workerform.blood-group.label"
                      defaultMessage="Blood Group"
                    />
                  }
                  name="bloodGroup"
                >
                  <Select
                    options={_.map(workers.bloodGroups, (r) => ({
                      label: r.name,
                      value: r.id,
                    }))}
                    showSearch
                    filterOption={(input, option) =>
                      option.label.toLowerCase().indexOf(input.toLowerCase()) >= 0
                    }
                  />
                </Form.Item>
              </Col>
            </Row>
            <Row>
              <Typography.Title level={5}>
                <FormattedMessage
                  id="s4.workerform.typography.contact-details.title"
                  defaultMessage="Contact Details"
                />
              </Typography.Title>
            </Row>
            <Row gutter={8}>
              <Col xs={24} sm={12} md={12} lg={8} xl={6}>
                <Form.Item
                  label={
                    <FormattedMessage id="s4.workerform.mobile.label" defaultMessage="Mobile" />
                  }
                  name="mobile"
                  rules={[
                    {
                      message: (
                        <FormattedMessage
                          id="s4.workerform.enter-mobile-number.message"
                          defaultMessage="Minimum lenght should be 5"
                        />
                      ),
                      min: 5,
                    },
                    {
                      message: (
                        <FormattedMessage
                          id="s4.workerform.enter-valid-mobile-number.message"
                          defaultMessage="Enter valid mobile number"
                        />
                      ),
                      pattern: /([0-9\s-]{7,})(?:\s*(?:#|x\.?|ext\.?|extension)\s*(\d+))?$/,
                    },
                    {
                      message: (
                        <FormattedMessage
                          id="s4.workerform.maximum-length-should-be-16.message"
                          defaultMessage="Maximum length should be 16 "
                        />
                      ),
                      max: 16,
                    },
                  ]}
                >
                  <Input
                    maxLength="16"
                    onKeyPress={(event) => {
                      if (!/[+, -, 0-9]/.test(event.key)) {
                        event.preventDefault();
                      }
                    }}
                  />
                </Form.Item>
              </Col>
              <Col xs={24} sm={12} md={12} lg={8} xl={6}>
                <Form.Item
                  label={
                    <FormattedMessage
                      id="s4.workerform.emergency-contact-name.label"
                      defaultMessage="Emergency Contact Name"
                    />
                  }
                  name="eName"
                  rules={[
                    {
                      message: (
                        <FormattedMessage
                          id="s4.workerform.enter-emergency-contact-name.message"
                          defaultMessage="Enter emergency contact name"
                        />
                      ),
                    },
                    {
                      message: (
                        <FormattedMessage
                          id="s4.workerform.enter-only-alphabets..message"
                          defaultMessage="Enter only alphabets."
                        />
                      ),
                      pattern: /^[a-zA-Z _]+$/,
                    },
                    {
                      required: false,
                      max: 80,
                      message: 'Name can be only a maximum of 80 characters',
                    },
                  ]}
                >
                  <Input />
                </Form.Item>
              </Col>
              <Col xs={24} sm={12} md={8} lg={8} xl={6}>
                <Form.Item
                  label={
                    <FormattedMessage
                      id="s4.workerform.emergency-contact-relation.label"
                      defaultMessage="Emergency Contact Relation"
                    />
                  }
                  name="eRelation"
                >
                  <Select
                    options={_.map(workers.workerRelations, (r) => ({
                      label: r.name,
                      value: r.id,
                    }))}
                    showSearch
                    filterOption={(input, option) =>
                      option.label.toLowerCase().indexOf(input.toLowerCase()) >= 0
                    }
                  />
                </Form.Item>
              </Col>

              <Col xs={24} sm={12} md={8} lg={12} xl={6}>
                <Form.Item
                  label={
                    <FormattedMessage
                      id="s4.workerform.emergency-mobile.label"
                      defaultMessage="Emergency Mobile"
                    />
                  }
                  name="eMobile"
                  rules={[
                    {
                      message: (
                        <FormattedMessage
                          id="s4.workerform.enter-mobile-number.message"
                          defaultMessage="Enter mobile number"
                        />
                      ),
                      min: 5,
                    },
                    {
                      message: (
                        <FormattedMessage
                          id="s4.workerform.enter-valid-mobile-number.message"
                          defaultMessage="Enter valid mobile number"
                        />
                      ),
                      pattern: /([0-9\s-]{7,})(?:\s*(?:#|x\.?|ext\.?|extension)\s*(\d+))?$/,
                    },
                    {
                      message: (
                        <FormattedMessage
                          id="s4.workerform.maximum-length-should-be-16.message"
                          defaultMessage="Maximum length should be 16"
                        />
                      ),
                      max: 16,
                    },
                  ]}
                >
                  <Input
                    onKeyPress={(event) => {
                      if (!/[+, -, 0-9]/.test(event.key)) {
                        event.preventDefault();
                      }
                    }}
                    maxLength="16"
                  />
                </Form.Item>
              </Col>
              <Col xs={24} sm={12} md={8} lg={12} xl={8}>
                <Form.Item
                  label={<FormattedMessage id="s4.workerform.email.label" defaultMessage="Email" />}
                  name="email"
                  rules={[
                    {
                      message: (
                        <FormattedMessage
                          id="s4.workerform.enter-only-number,alphabets-and-alphanumeric..message"
                          defaultMessage="Enter only number, alphabets and alphanumeric."
                        />
                      ),
                      pattern: /[a-zA-Z0-9][a-zA-Z0-9._-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/,
                    },
                  ]}
                >
                  <Input />
                </Form.Item>
              </Col>
              <Col xs={24} sm={12} md={12} lg={12} xl={8}>
                <Form.Item
                  label={
                    <FormattedMessage id="s4.workerform.address.label" defaultMessage="Address" />
                  }
                  name="address"
                  rules={[
                    {
                      required: false,
                      max: 240,
                      message: (
                        <FormattedMessage
                          id="s4.workerform.address-can-be-only-a-maximum-of-240-characters.message"
                          defaultMessage="Address can be only a maximum of 240 characters"
                        />
                      ),
                    },
                  ]}
                >
                  <Input.TextArea rows={1} />
                </Form.Item>
              </Col>

              <Col xs={24} sm={12} md={12} lg={12} xl={8}>
                <Form.Item
                  label={
                    <FormattedMessage id="s4.workerform.comments.label" defaultMessage="Comments" />
                  }
                  name="description"
                  rules={[
                    {
                      required: false,
                      max: 400,
                      message: (
                        <FormattedMessage
                          id="s4.workerform.comments-can-be-only-a-maximum-of-400-characters.message"
                          defaultMessage="Comments can be only a maximum of 400 characters"
                        />
                      ),
                    },
                  ]}
                >
                  <Input.TextArea rows={1} />
                </Form.Item>
              </Col>
            </Row>
          </>

          <Col span={24}>
            <Form.Item>
              <Button
                type="primary"
                htmlType="submit"
                style={{ width: '100%' }}
                loading={loading.effects['workers/addWorker']}
                disabled={!isEdited}
              >
                {getAddButtonText()}
              </Button>
            </Form.Item>
          </Col>
        </Form>
      </Modal>
    </>
  );
};

export default connect(({ devices, loading, workers, sites, user, worker }) => ({
  devices,
  loading,
  workers,
  sites,
  user,
  worker,
}))(WorkerForm);
